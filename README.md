## !! As of 18-09-2017 this project has been moved to GitHub !!
As of 18-09-2017 this project has been moved to GitHub: https://github.com/denxorz/HotChocolatey


## Welcome to Hot Chocolatey

Hot chocolatey is a graphical user interface (GUI) for Chocolatey. 


### Features

Hot chocolatey currently has only a few basic features.

* Show installed packages
* Show available updates
* Search for packages
* Show package information
* Install/Update/Uninstall packages
* Share/filter-on a list of installed programs


### Source code

The source code can be found here: https://gitlab.com/jjb3/HotChocolatey

### Chocolatey package

Hot Chocolatey can be found as a package in Chocolatey: https://chocolatey.org/packages/hot-chocolatey/

### Installer

6.0.0 http://hotchocolatey.jjb3.nl/releases/6.0.0.0/Setup%20Hot%20Chocolatey.msi

5.0.0 http://hotchocolatey.jjb3.nl/releases/5.0.0.1016/Setup%20Hot%20Chocolatey.msi

4.0.0 http://hotchocolatey.jjb3.nl/releases/4.0.0.605/Setup%20Hot%20Chocolatey.msi

3.2.2 http://hotchocolatey.jjb3.nl/releases/3.2.2.516/Setup%20Hot%20Chocolatey.msi

3.1.0 http://hotchocolatey.jjb3.nl/releases/3.1.0.427/Setup%20Hot%20Chocolatey.msi

2.0.0 http://hotchocolatey.jjb3.nl/releases/2.0.0.830/Setup%20Hot%20Chocolatey.msi

1.0.0 http://hotchocolatey.jjb3.nl/releases/1.0.0.20150722/Setup%20Hot%20Chocolatey.msi